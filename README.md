## About
This program is a JSON pretty print formatter built upon the [Parson](https://github.com/kgabis/parson) library.

It takes a JSON file as input and writes the formatted JSON to the standard output.

The input to the program can also be provided from the standard input, this being very useful if the program wants to be used as a filter command or as an external formatter for a text editor.

Source code is standard C and should be compilable in any POSIX environment.

## Installation
Run ```make``` to compile, then run ```make install``` as root user to install the executable.

Default program location will be ```/usr/local/bin/json-pretty-print```, a custom destination directory for the executable can be specified by setting variable DESTDIR while building with make.

## Examples
Given the following file ```test.json```:
```
{"a":"b","c":2}
```
the program can be executed with the file name as input:
```
json-pretty-print test.json
```
or streamed from the standard input:
```
json-pretty-print < test.json
```
producing on the standard output:
```
{
	"a": "b",
	"c": 2
}
```

## License
[The MIT License (MIT)](http://opensource.org/licenses/mit-license.php)
