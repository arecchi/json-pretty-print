/*
 Copyright (c) 2018 Raffaele Arecchi
  
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
        
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
         
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "parson.h"

#define BUF_SIZE 1024

static char *readFile(char *filename) {
	FILE *f = fopen(filename, "rt");
	assert(f);
	fseek(f, 0, SEEK_END);
	long length = ftell(f);
	fseek(f, 0, SEEK_SET);
	char *buffer = (char *) malloc(length + 1);
	buffer[length] = '\0';
	fread(buffer, 1, length, f);
	fclose(f);
	return buffer;
}
 
int main(int argc, char *argv[])
{
	char *content;

	if (argc == 1)
	{
		content = (char*) malloc(sizeof(char) * BUF_SIZE);
		char buffer[BUF_SIZE];
		size_t contentSize = 1; // includes NULL
	
		content[0] = '\0';
		while(fgets(buffer, BUF_SIZE, stdin))
		{
			contentSize += strlen(buffer);
			content = realloc(content, contentSize);
			strcat(content, buffer);
		}

		if(ferror(stdin))
		{
			free(content);
			perror("Error reading from stdin.");
			return 1;
		}
	}
	else
	{
		content = readFile(argv[1]);
	}

	JSON_Value *root = json_parse_string(content);
	if (root == NULL)
	{
		free(content);
		perror("Error: the content does not look to be JSON format");
		return 1;
	}
	char *serialized_string = json_serialize_to_string_pretty(root);
	puts(serialized_string);
	json_free_serialized_string(serialized_string);
	json_value_free(root);
	free(content);
	return 0;
}
