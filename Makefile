CC = c99
CFLAGS = -O0 -g -Wall -Wextra -std=c99 -pedantic-errors
PREFIX=/usr/local

all: json-pretty-print

parson.o: parson.c parson.h
	$(CC) $(CFLAGS) -c parson.c

json-pretty-print.o: parson.h json-pretty-print.c
	$(CC) $(CFLAGS) -c json-pretty-print.c

json-pretty-print: json-pretty-print.o parson.o
	$(CC) $(CFLAGS) json-pretty-print.o parson.o -o json-pretty-print

clean:
	rm -f *.o json-pretty-print

install: json-pretty-print
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -pf json-pretty-print $(DESTDIR)$(PREFIX)/bin

